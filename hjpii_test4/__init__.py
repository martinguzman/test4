# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .test4 import *

def register():
    Pool.register(
        Test4,
        module='hjpii_test4', type_='model')
