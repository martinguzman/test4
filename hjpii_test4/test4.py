# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSQL, fields

__all__ = ['Test4']

class Test4(ModelSQL, ModelView):
    "Test4"
    __name__ = "hjpii.test4"
    
    patient = fields.Many2One('party.party', 'patient',required=True)


